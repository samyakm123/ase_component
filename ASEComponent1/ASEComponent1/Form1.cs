﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


/// <summary>
///  Samyak Manandhar
///  ID: C7227216
///  College: The British College
/// </summary>

namespace ASEComponent1
{
    /// <summary>
    /// Form 1 main Form containing all interactions
    /// </summary>

    public partial class Form1 : Form
    {
        String input;

        /// <summary>
        /// declaring a graphics object g
        /// </summary>
        public Graphics g;
        Bitmap drawOutput;  
        /// <summary>
        /// method to initialize components
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            drawOutput = new Bitmap(pictureBox.Width, pictureBox.Height);
            g = Graphics.FromImage(drawOutput);

        }

        private void programBox_TextChanged(object sender, EventArgs e)
        {
            input = programBox.Text;
        }

        ///<summary>
        ///Button executes the command from command line
        ///</summary>

        private void enterButton_Click_1(object sender, EventArgs e)
        {

            String cmdLine = commandBox.Text;

          

            if (!String.IsNullOrEmpty(cmdLine))
            {
               
                
                for (int i = 0; i < programBox.Lines.Length; i++)
                {
                    String textLine = programBox.Lines[i];

                    if (cmdLine.Equals("run"))
                    {
                        RunProgram(textLine);
                        historyBox.Visible = true;
                        historyBox.Text += "\r\n" + textLine;
                    }
                }


                if (cmdLine == "clear")
                {
                    
                    g.Clear(DefaultBackColor);
                    pictureBox.Image = drawOutput;
                    MessageBox.Show("Your Picture box is cleared");

                }


                else if (cmdLine == "reset")
                {
                    ShapeProperties.setCo(0, 0);
                }

                programBox.Clear();
            }
            else
            {
                MessageBox.Show("Command Line is empty");
            }

        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {

        }

        ///<summary>
        ///This method calls parse method from commands class
        ///</summary>

        public void RunProgram(String textLine)
        {

         
            Commands cmd = new Commands();
            cmd.Parse(textLine, g);
            

            pictureBox.Image = drawOutput;


        }

        ///<summary>
        ///This event closes the program from strip menu
        ///</summary>

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        ///<summary>
        ///This Keyevent press the enter button with Enter key from keyboard
        ///</summary>

        private void commandBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                enterButton.PerformClick();
            }
        }

        ///<summary>
        ///This event saves the program into the local drive
        ///</summary>

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter fWriter = File.CreateText(saveFileDialog.FileName);
                fWriter.WriteLine(historyBox.Text);
                fWriter.Close();
            }


        }

        ///<summary>
        ///This event loads the program from the local drive
        ///</summary>

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                OpenFileDialog openFileDialog = new OpenFileDialog();

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamReader s = File.OpenText(openFileDialog.FileName);
                    programBox.Text += s.ReadToEnd();
                }
                

            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Error", "Cannot find the file");
            }
            catch (IOException ie)
            {
                MessageBox.Show("Error", "IO exception");
            }
        }

        

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Form1().Show();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //new Form2().Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }
    }
}
