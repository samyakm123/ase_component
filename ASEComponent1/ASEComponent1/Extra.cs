﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/// <summary>
///  Samyak Manandhar
///  ID: C7227216
///  College: The British College
/// </summary>

namespace ASEComponent1
{
    /// <summary>
    /// class to detect variable and store data in variable
    /// </summary>
    public static class Extra
    {

        static String command, command2;
        /// <summary>
        /// defining variables to assign variables entered by users
        /// </summary>
        public static int rad, wid, hig, cot, bas, tig;

        /// <summary>
        /// method to check if the line contains variables to be stored
        /// </summary>
        /// <param name="cmd"></param>

        public static void Parse2(String cmd)

        {
            cmd = cmd.ToLower().Trim();

            try
            {

                if (cmd.Contains('=') && !cmd.Contains('+'))
                {
                    string[] split = cmd.Split('=');
                    command = split[0];
                    command2 = split[1];

                    if (command == "radius")
                    {
                        rad = Int32.Parse(command2);
                    }

                    else if (command == "width")
                    {
                        wid = Int32.Parse(command2);
                    }
                    else if (command == "height")
                    {
                        hig = Int32.Parse(command2);
                    }
                    else if (command == "count")
                    {
                        cot = Int32.Parse(command2);
                    }
                    else if (command == "base")
                    {
                        bas = Int32.Parse(command2);
                    }
                    else if (command == "theight")
                    {
                        tig = Int32.Parse(command2);
                    }
                }
            }
            catch (InvalidProgramException e)
            {
                MessageBox.Show("Please check your code and try again");
            }   
                    
                
            
        }
    }
}
