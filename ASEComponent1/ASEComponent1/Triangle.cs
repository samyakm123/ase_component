﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

/// <summary>
///  Samyak Manandhar
///  ID: C7227216
///  College: The British College
/// </summary>

namespace ASEComponent1
{

    /// <summary>
    /// this class inherits property from shape properties to draw rectangle
    /// </summary>

    public class Triangle: ShapeProperties
    {
        static int btm, height;

        /// <summary>
        /// constructor to initialize base and height of traingle
        /// </summary>
        /// <param name="btm"></param>
        /// <param name="height"></param>

        public Triangle(int btm, int height)
        {
            Triangle.btm = btm;
            Triangle.height = height;
        }

        /// <summary>
        /// method to draw a triangle which will be called in commands class
        /// </summary>
        /// <param name="g"></param>

        public static void drawShape(Graphics g)
        {
            PointF point1 = new PointF(x, height);
            PointF point2 = new PointF(btm, height);
            PointF point3 = new PointF((btm / 2), x);
            PointF[] polygonpoints = { point1, point2, point3 };

            if(fill == true)
            {
                g.FillPolygon(brush, polygonpoints);
            }
            else
            {
                g.DrawPolygon(p, polygonpoints);
            }
            
        }
    }
}
