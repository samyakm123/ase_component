﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

/// <summary>
///  Samyak Manandhar
///  ID: C7227216
///  College: The British College
/// </summary>

namespace ASEComponent1
{

    /// <summary>
    /// This class pass the parse constructor to form1
    /// </summary>

    public class Commands
    {
        String command, command2, command3;
        /// <summary>
        /// defined variables to pass values to other class
        /// </summary>
        public int val1, val2;
                    
        
        /// <summary>
        /// This method calls method from shape classes and shapeproperties class
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="g"></param>

        public void Parse(String cmd, Graphics g)
        {
           
            Extra.Parse2(cmd);
            
            
            try
            {
                
                if (cmd.Contains(','))
                {
                   
                    string[] split = cmd.Split(' ');

                    command = split[0];
                    String[] values = split.Last().Split(',');
                    command2 = values[0];
                    command3 = values[1];


                    

                    if (command == "rectangle")
                    {
                        if (command2 == "width" && command3 == "height")
                        {
                            val1 = Extra.wid;
                            val2 = Extra.hig;
                            MessageBox.Show("asdf" + Extra.wid + val2 + "inside");
                        }
                        else
                        {
                            val1 = Int32.Parse(command2);
                            val2 = Int32.Parse(command3);
                        }

                        new Rectangle(val1, val2);
                        Rectangle.drawShape(g);
                    }

                    if (command == "triangle")
                    {

                        val1 = Int32.Parse(command2);
                        val2 = Int32.Parse(command3);

                        new Triangle(val1, val2);
                        Triangle.drawShape(g);
                    }


                    if (command == "moveto")
                    {

                        val1 = Int32.Parse(command2);
                        val2 = Int32.Parse(command3);

                        ShapeProperties.setCo(val1, val2);
                    }

                }
                else if (!cmd.Contains(","))
                {
                    string[] split = cmd.Split(' ');
                    foreach (String words in split)
                    {
                        command = split[0];
                        command2 = split[1];

                        if (command == "pen")
                        {
                            if (command2 == "red")
                            {
                                ShapeProperties.setColor(Color.Red);
                            }
                            else if (command2 == "green")
                            {
                                ShapeProperties.setColor(Color.Green);
                            }
                            else if (command2 == "yellow")
                            {
                                ShapeProperties.setColor(Color.Yellow);
                            }
                            else if (command2 == "blue")
                            {
                                ShapeProperties.setColor(Color.Blue);
                            }
                            else if (command2 == "purple")
                            {
                                ShapeProperties.setColor(Color.Purple);
                            }
                            else
                            {
                                MessageBox.Show("Try a different color for pen.");
                            }
                            continue;
                        }

                        if (command == "fill")
                        {
                            if (command2 == "true")
                            {
                                ShapeProperties.fillShape(true);
                            }
                            else if (command2 == "false")
                            {
                                ShapeProperties.fillShape(false);
                            }
                            else
                            {
                                MessageBox.Show("fill should be true or flase only. Try Again");
                            }
                            continue;
                        }

                        

                        if (command == "circle")
                        {
                            val1 = Int32.Parse(command2);

                            new Circle(val1);
                            Circle.drawShape(g);
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Check your parameters & Try Again");
                }
            }
            catch
            {
                MessageBox.Show("Please check your program & Try Again");
            }

        }

    }
}
