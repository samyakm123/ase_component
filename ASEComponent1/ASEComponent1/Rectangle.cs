﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

/// <summary>
///  Samyak Manandhar
///  ID: C7227216
///  College: The British College
/// </summary>

namespace ASEComponent1
{
    /// <summary>
    /// this class inherits property from shape properties to draw rectangle
    /// </summary>

    public class Rectangle : ShapeProperties
    {

        static int width, height;

        /// <summary>
        /// constructor to initialize width and height of rectangle
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>

        public Rectangle(int width, int height)
        {
            Rectangle.width = width;
            Rectangle.height = height;
        }

        /// <summary>
        /// method to draw a rectangle which will be called in commands class
        /// </summary>
        /// <param name="g"></param>

        public static void drawShape(Graphics g)
        {
            if (fill == true)
            {
                g.FillRectangle(brush, x, y, width, height);
            }
            else
            {
                g.DrawRectangle(p, x, y, width, height);
            }
        }
    }
}
