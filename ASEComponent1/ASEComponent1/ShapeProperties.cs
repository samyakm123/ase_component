﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

/// <summary>
///  Samyak Manandhar
///  ID: C7227216
///  College: The British College
/// </summary>

namespace ASEComponent1
{
    /// <summary>
    /// This class passes data from command to shape classes
    /// </summary>

    public class ShapeProperties
    {
         
        /// <summary>
        /// making a SolidBrush object
        /// </summary>

        public static SolidBrush brush = new SolidBrush(Color.Black);

        /// <summary>
        /// declaring variables x and y 
        /// </summary>
        public static int x, y;

        /// <summary>
        /// creating a pen object
        /// </summary>

        public static Pen p = new Pen(Color.Black);

        /// <summary>
        /// declaring a boolean variable
        /// </summary>

        public static Boolean fill;

        /// <summary>
        /// method to set pen color and fill color from the command class
        /// </summary>
        /// <param name="color"></param>

        public static void setColor(Color color)
        {
            p = new Pen(color);
            brush = new SolidBrush(color);
        }

        /// <summary>
        /// method to set the draw coordinates from the command class
        /// </summary>
        /// <param name="val1"></param>
        /// <param name="val2"></param>

        public static void setCo(int val1, int val2)
        {
            x = val1;
            y = val2;
        }

        /// <summary>
        /// method to fill the shapes or not from the command class
        /// </summary>
        /// <param name="fill"></param>

        public static void fillShape(Boolean fill)
        {
            ShapeProperties.fill = fill;
        }
    }
}
