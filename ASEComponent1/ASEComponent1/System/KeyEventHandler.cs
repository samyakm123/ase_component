﻿using System.Windows.Forms;

namespace System
{
    internal class KeyEventHandler
    {
        private Action<object, KeyEventArgs> commandBox_TextChanged;

        public KeyEventHandler(Action<object, KeyEventArgs> commandBox_TextChanged)
        {
            this.commandBox_TextChanged = commandBox_TextChanged;
        }
    }
}