﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

/// <summary>
///  Samyak Manandhar
///  ID: C7227216
///  College: The British College
/// </summary>

namespace ASEComponent1
{

    /// <summary>
    /// this class inherits property from shape properties to draw circle 
    /// </summary>

    public class Circle: ShapeProperties
    {
        static int radius;

        /// <summary>
        /// constructor to initialize radius of circle
        /// </summary>
        /// <param name="radius"></param>

        public Circle(int radius)
        {
            Circle.radius = radius;
        }

        /// <summary>
        /// Method to draw a circle which will be called in commands class
        /// </summary>
        /// <param name="g"></param>

        public static void drawShape(Graphics g)
        {
            if (fill == true)
            {
                g.FillEllipse(brush, x, y, (radius*2), (radius * 2));
            }
            else
            {
                g.DrawEllipse(p, x, y, (radius * 2), (radius * 2));
            }
        }
    }
}
