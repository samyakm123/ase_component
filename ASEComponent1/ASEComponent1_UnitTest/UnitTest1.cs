﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ASEComponent1;

/// <summary>
///  Samyak Manandhar
///  ID: C7227216
///  College: The British College
/// </summary>

namespace ASEComponent1_UnitTest
{
    [TestClass]

    ///<summary>
    ///class for unit testing 
    /// </summary>

    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
         
            ///<summary>
            ///testing if the rectangle class is set or not
            /// </summary>
            
            Rectangle rect1 = new Rectangle(20, 50);
            Assert.IsNotNull(rect1);
            Assert.IsInstanceOfType(rect1, typeof(Rectangle));

            ///<summary>
            ///testing if the circle class is set or not
            /// </summary>

            Circle circ1 = new Circle(20);
            Assert.IsNotNull(circ1);
            Assert.IsInstanceOfType(circ1, typeof(Circle));

            ///<summary>
            ///testing if the triangle class is set or not
            /// </summary>

            Triangle tri1 = new Triangle(20, 50);
            Assert.IsNotNull(tri1);
            Assert.IsInstanceOfType(tri1, typeof(Triangle));

        }

        
    }
}
